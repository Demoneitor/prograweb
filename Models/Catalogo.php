<?php

require 'Model.php';


class name extends Model {
	
	function __construct() {
		parent::__construct();
		$this->table = 'name';
		$this->columns = ['identi'];
	}

	public function add($values) {
		return parent::add($values);
	}

	public function edit($cols, $condition) {
		return parent::edit($cols, $condition);
	}

	public function findAll() {
		return parent::findAll();
	}

	public function find($cols, $condition) {
		return parent::find($cols, $condition);
	}

	public function remove($condition) {
		return parent::remove($condition);
	}

}

?>
